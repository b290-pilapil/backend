// CRUD Operations
    // Create
        // instert one
        // db.connectionName.insertOne({object})
        db.users.insertOne({
            firstName: "Jane",
            lastName: "Doe",
            age: 21,
            contact: {
                phone: "09123456789",
                email: "janedoe@gmail.com"
            },
            courses: ["CSS", "JavaScript", "Python"],
            department: "none"
        })

        // insert multiple
        // db.connectionName.insertMany([{object}])
        db.users.insertMany([
            {
                firstName: "Stephen",
                lastName: "Hawking",
                age: 76,
                contact: {
                    phone: "09123456789",
                    email: "stephenhawking@gmail.com"
                },
                courses: ["Python", "React", "PHP"],
                department: "none" 
            },
            {
                firstName: "Neil",
                lastName: "Armstrong",
                age: 82,
                contact: {
                    phone: "09123456789",
                    email: "neilarmstrong@gmail.com"
                },
                courses: ["React", "Laravel", "Sass"],
                department: "none"
            }
        ])

    // Read
        // get all
        // db.collectionName.find()
        db.users.find()
        // with param
        // db.collectionName.find({field: value})
        db.users.find({lastName: "Hawking"})
        // multiple params
        // db.collectionName.find({fieldA: valueA},{fieldB: valueB})
        db.users.find({lastName: "Armstrong", age:82})

        // findOne
            // same param stucture

    // Update
        // update one
            // akin to patching

            // test dummmy
            db.users.insertOne({
                firstName: "test",
                lastName: "test",
                age: 0,
                contact: {
                    phone: "00000000000",
                    email: "test@gmail.com"
                },
                courses: [],
                department: "none"
            })
        // db.collectionName.updateOne({criteria}, {$set: {field: value}})
            // updates 1st match only
        db.users.updateOne(
            {firstName:"test"},
            {
                $set: {
                    firstName: "bill",
                    lastName: "gates",
                    age: 65,
                    contact: {
                        phone: "09123456789",
                        email: "bill@gmail.com"
                    },
                    courses: ["PHP", "Laravel", "HTML"],
                    department: "Operations",
                    status: "active"            
                }
            }
        )
        // updating multiple
        // db.collectionName.updateMany({criteria}, {$set: {fieldA: value}})
        db.users.updateMany(
            {department: "none"},
            {$set: {department: "HR"}}
        )
        // Replace one
            // replace document fields and value
                // entire document is needed
        db.users.replaceOne(
            {firstName: "Bill"},
            {
                $set: {
                    firstName: "Bill",
                    lastName: "Gates",
                    age: 65,
                    contact: {
                        phone: "09123456789",
                        email: "bill@gmail.com"
                    },
                    courses: ["PHP", "Laravel", "HTML"],
                    department: "Operations",
                }
            }
        )

    // Delete
        // Delete one
            // 1st match again
        // db.collectionName.deleteOne({criteria})
        db.users.deleteOne({firstName: "test"})

        // Delete many
            // be careful, possible DeleteAll scenario => no criteria
        // db.collectionName.deleteMany({criteria})
        db.users.deleteMany({firstName:"Bill"})

// Advanced Queries
    db.users.find({
        contact: {
            phone: "09123456789",
            email: "stephenhawking@gmail.com"
        }
    })

    db.users.find({"contact.email": "janedoe@gmail.com"})
    db.users.find({courses: ["CSS", "JavaScript", "Python"]}) //needs exact order
    db.users.find({courses: {$all: ["React", "Python"]}}) //any?

    // Querying embedded array
    db.users.insertOne({
        nameArr: [
            {
                nemea: "juan"
            },
            {
                nameb: "tamad"
            }
        ]
    })

    // mind the typo ahahhahah
    db.users.find({
        nameArr: {
            namea: "juan"
        }
    })