console.log("hello uwu")

// arith ops
let x =1397
let y = 7831
let sum = x+y
console.log("Result of addition: "+sum)
let diff = x-y
console.log("Result of subtraction: "+diff)
let prod = x*y
console.log("Result of multiplication: "+prod)
let quotient = x/y
console.log("Result of division: "+quotient)
let mod = x%y
console.log("Result of modulo: "+mod)


// console.log(0.1+0.2)

// assignment ops
    //  assignmnet op (=)
    // The assignment operator assigns the value of the **right** operand to a variable.
    let assignmentNumber = 8
    console.log("result of =: "+assignmentNumber)

    // addition assignment op (+=)
    // The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.
    assignmentNumber +=2
    console.log("result of assignmentNumber +=2: "+assignmentNumber)

    // -=
    assignmentNumber -=5
    console.log("result of assignmentNumber -=5: "+assignmentNumber)
    // *=
    assignmentNumber *=10
    console.log("result of assignmentNumber *=10: "+assignmentNumber)
    //   /=  
    assignmentNumber /=10
    console.log("result of assignmentNumber /=10: "+assignmentNumber)
    // %=
    assignmentNumber %=3
    console.log("result of assignmentNumber %=3: "+assignmentNumber)

    let mdas = 1+2-3*4/5
    console.log("mdas: " +mdas)

    let pemdas = 1+(2-3)*(4/5)
    console.log("pemdas: "+pemdas)
    pemdas = (1+(2-3)*(4/5))
    console.log("pemdas: "+pemdas)

    // increment or decrement
    // Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to
    let z = 1
    // The value of "z" is added by a value of one before returning the value and storing it in the variable "increment"
    let increment = ++z
    console.log("res pre++: "+increment)
    // The value of "z" was also increased even though we didn't implicitly specify any value reassignment
    console.log("res pre++ (z): "+z)

    increment = z++
    // The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
    console.log("res post++: "+increment)
    // The value of "z" is at 2 before it was incremented
    console.log("res post++ (z): "+z)
    
    let decrement = --z
    // The value of "z" is decreased by a value of one before returning the value and storing it in the variable "decrement"
    console.log("res pre--: "+decrement)
    // The value of "z" is at 3 before it was decremented
    console.log("res pre-- (z): "+z)
    // The value of "z" was decreased reassigning the value to 2

    
    // The value of "z" is returned and stored in the variable "increment" then the value of "z" is decreased by one
    decrement = --z
    // The value of "z" is at 2 before it was decremented
    console.log("res post--: "+decrement)
    // The value of "z" was decreased reassigning the value to 1
    console.log("res post-- (z): "+z)

// comparison ops
let juan = 'juan'
    // loose equality (==)
    console.log(1==1)
    console.log(1==2)
    console.log(1=='1')
    console.log(0==false)
    console.log('juan'=="juan")
    console.log(juan=="juan")
    // strict equality (===)
    /* 
        - Checks whether the operands are equal/have the same content
        - Also COMPARES the data types of 2 values
        - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
        - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
        - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
        - Strict equality operators are better to use in most cases to ensure that data types provided are correct
    */
    console.log("-------------------")
    console.log(1===1)
    console.log(1===2)
    console.log(1==='1')
    console.log(0===false)
    console.log('juan'==="juan")
    console.log(juan==="juan")
    // loose inequality (!=)
    /* 
        - Checks whether the operands are not equal/have different content
        - Attempts to CONVERT AND COMPARE operands of different data types
    */
    console.log("-------------------")
    console.log(1!=1)
    console.log(1!=2)
    console.log(1!='1')
    console.log(0!=false)
    console.log('juan'!="juan")
    console.log(juan!="juan")
    // strict inequality
    /* 
        - Checks whether the operands are not equal/have the same content
        - Also COMPARES the data types of 2 values
    */
    console.log("-------------------")
    console.log(1!==1)
    console.log(1!==2)
    console.log(1!=='1')
    console.log(0!==false)
    console.log('juan'!=="juan")
    console.log(juan!=="juan")

// relational ops
//Some comparison operators check whether one value is greater or less than to the other value.
    let a = 50
    let b = 65
    
    let isGT = a>b
    let isLT = a<b
    let isGTE = a>=b
    let isLTE = a<=b
    console.log("-------------------")
    console.log(isGT)
    console.log(isLT)
    console.log(isGTE)
    console.log(isLTE)

    let numStr = '30'
    console.log("-------------------")
    console.log(a > numStr) //numstr forced to int
    console.log(b <= numStr)

    let str = 'twenty'
    console.log("-------------------")
    console.log(a > str) //str is not numeric (NaN)
    console.log(b <= str)

// logical ops
    let isLegal = true
    let isRegis = false

    // &&
    let allReqsMet = isLegal && isRegis
    console.log("-------------------")
    console.log("allReqs (&&): "+allReqsMet)

    // ||
    let someReqsMet = isLegal || isRegis
    console.log("someReqs (||): "+someReqsMet)

    // !
    let someReqsNotMet= !isRegis
    console.log("someReqsNot (!): "+someReqsNotMet)