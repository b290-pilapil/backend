const Course = require('../models/Course')

// Create a new course
    // Steps:
    // 1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
    // 2. Save the new User to the database
module.exports.addCourse = reqBody => {
    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    })
    // save the created object
    return newCourse.save()
        .then(course=> true)
        .catch(error => {
            console.log(error)
            return false
        })
}

module.exports.getAllCourses = ()=>{
    return Course.find({})
        .then(result => result)
        .catch(err=>console.log(err))
}

// Retrieve all ACTIVE courses
    // Steps:
    // 1. Retrieve all the courses from the database with the property of "isActive" to true
module.exports.getAllActiveCourses = ()=>{
    return Course.find({isActive: true})
        .then(result => result)
        .catch(err=>console.log(err))
}

module.exports.getCourse = reqParams=>{
    return Course.findById(reqParams.courseId)
    .then(result => result)
    .catch(err=>console.log(err))
}

module.exports.updateCourse = (reqParams, reqBody)=>{
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }
    // Syntax
        // findByIdAndUpdate(document ID, updatesToBeApplied)
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
        .then(result => true)
        .catch(err=>console.log(err))
}

module.exports.archiveCourse= (reqParams, reqBody)=>{
    return Course.findByIdAndUpdate(reqParams.courseId, {isActive: reqBody.isActive})
        .then(result => true)
        .catch(err=>console.log(err))
}