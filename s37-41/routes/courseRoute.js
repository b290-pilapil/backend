const express = require('express')
const router = express.Router()


const auth = require('../auth')
const courseController = require('../controllers/courseController')

// route for creating a course
router.post("/", auth.verify, (req, res)=>{
    const userData = auth.decode(req.headers.authorization)
    // console.log(userData)
    if(userData.isAdmin===true){
        courseController.addCourse(req.body)
            .then(controllerResult => res.send(controllerResult))
    }else{
        res.send("Cannot create course. User is not an admin")
    }
})

    // Steps:
    // 1. Retrieve all the courses from the database
router.get("/all", auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin===true){
        courseController.getAllCourses()
            .then(controllerResult => res.send(controllerResult))
    }else{
        res.send("Cannot view. User is not an admin")
    }
})


router.get("/", (req,res)=>{
    courseController.getAllActiveCourses()
        .then(controllerResult => res.send(controllerResult))
})
// Route for retrieving a specific course
    // Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
router.get("/:courseId", (req,res)=>{
    // Since the course ID will be sent via the URL, we cannot retrieve it from the request body
    courseController.getCourse(req.params)
        .then(controllerResult => res.send(controllerResult))
})
// Route for updating a course
		// JWT verification is needed for this route to ensure that a user is logged in before updating a course
router.put("/:courseId", auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin===true){
        courseController.updateCourse(req.params, req.body)
            .then(controllerResult => res.send(controllerResult))
    }else{
        res.send("Cannot update. User is not an admin")
    }
})

router.patch("/:courseId/archive", auth.verify, (req, res)=>{
    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin===true){
        courseController.archiveCourse(req.params, req.body)
            .then(controllerResult => res.send(controllerResult))
    }else{
        res.send("Cannot archive. User is not an admin")
    }
})

module.exports = router