// wla akong sample code kasi nag type ako nito sa phone
// maraming interruption eh XD

// arrays
    // Arrays are used to store multiple related values in a single variable
    // They are declared using square brackets ([]) also known as "Array Literals"
    // Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
    // Arrays also provide access to a number of functions/methods that help in achieving this
    // A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
    // Majority of methods are used to manipulate information stored within the same object
    // Arrays are also objects which is another data type
    // The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"
    // Syntax
    //     let/const arrayName = [elementA, elementB, ElementC...]

    // can contain mixed data type

// array.length
    // decrement length to delete tail elements

    // DISCLAIMER
        // doesnt work with string

    // increment to create space for a new element

// reading from arrays
    // Accessing array elements is one of the more common tasks that we do with an array
    // This can be done through the use of array indexes
    // Each element in an array is associated with it's own index/number
    // In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
    // The reason an array starts with 0 is due to how the language is designed
    // Array indexes actually refer to an address/location in the device's memory and how the information is stored
    // Example array location in memory
    //     Array address: 0x7ffe9472bad0
    //     Array[0] = 0x7ffe9472bad0
    //     Array[1] = 0x7ffe9472bad4
    //     Array[2] = 0x7ffe9472bad8
    // In the example above, the first element and the array itself points to the same memory location and therefore is at 0 elements away from the location of the array itself
    // Syntax
    //     arrayName[index];

    // out of bounds returns underfined
    //Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element.

// adding items
    // using the index (risk of overwriting)

// loops
    //You can use a for loop to iterate over all items in an array.
    //Set the counter as the index and set a condition that as long as the current index iterated is less than the length of the array, we will run the condition. It is set this way because the index of an array starts at 0.

// MD array
    // Multidimensional arrays are useful for storing complex data structures
    // A practical application of this is to help visualize/create real world objects
    // Though useful in a number of cases, creating complex array structures is not always recommended.

// accessing MD arr elems
    // arr[][]