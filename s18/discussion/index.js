function printInput(){
	let nickname = prompt("Enter your nickname:")
	return "Hi, "+nickname
}
// console.log(printInput())

function printName(name){
	return "Hi, "+name
}
console.log(printName("clyde"))

function printInput2(nickname){
	nickname = prompt("Enter your nickname:")
	return "Hi, "+nickname
}
// console.log(printInput2("nickname"))

// You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.
	// "name" is called a parameter.

	// A "parameter" acts as a named variable/container that exists only inside of a function
	// It is used to store information that is provided to a function when it is called/invoked.

	// the information/data provided directly into the function, is called an argument. Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.

// parameters with the prompt don't need hardcoded arguments since the values will be provided via prompt

function checkDivisibiliyBy8(num){
	let rem = num%8
	console.log(num+" % 8: "+rem)

	let isDivisibleby8= rem===0
	console.log(num+ " divisible by 8?")
	console.log(isDivisibleby8)
}
checkDivisibiliyBy8(64)
checkDivisibiliyBy8(28)

// Function parameters can also accept other functions as arguments
// Some complex functions use other functions as arguments to perform more complicated results.
// This will be further seen when we discuss array methods.

function argFunc(){
	console.log("arg func log")
}

function invokeFunc(argFuncParam){
	argFuncParam()
	console.log("invoker")
}
console.log(argFunc)
console.log(argFunc())
invokeFunc(argFunc)
// invokeFunc(argFunc())

// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.
function createFullName(fN, lN, Mi){
	return fN +" "+Mi+". "+lN
}
console.log(createFullName("juan", "dela cruz", "f"))

function tryMe(){
	console.log("Gulat ka noh?");
}