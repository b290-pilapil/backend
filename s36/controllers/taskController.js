// Controllers contain the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file

// Uses the "require" directive to allow access to the "Task" model which allows us to access Mongoose methods to perform CRUD functions
// Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require('../models/Task')

// The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed
module.exports.getAllTasks = ()=>{
    // The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed
    // The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman
    return Task.find({}).then(result => {
        // The "return" statement returns the result of the MongoDB query to the "result" parameter defined in the "then" method
        return result
    })
}

// Controller function for creating a task
// The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (reqBody)=>{
    // Creates a task object based on the Mongoose model "Task"
    let newTask = new Task ({
        // Sets the "name" property with the value received from the client/Postman
        name: reqBody.name
    })
    return newTask.save().then((task, error)=>{
        if(error){
            console.log(error)
            return false
        }else{
            return task
        }
    })
}

module.exports.deleteTask = (taskId)=>{
    // Unlike findByIdAndDelete(), the removed document is still accessible after the method is executed. It allows you to access the removed document, even after the operation, for any further processing or reference.
    return Task.findByIdAndRemove(taskId)
        .then(removedTask => {
            return removedTask
        }).catch(error => {
            console.log(error)
            return false
        })
}

    // Controller function for updating a task
		// Business Logic
		// 	1. Get the task with the id using the Mongoose method "findById"
		// 	2. Replace the task's status returned from the database with the "name" property from the request body
		// 	3. Save the task
		// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
		// The updates to be applied to the document retrieved from the "req.body" property coming from the client is renamed as "newContent"
module.exports.updateTask= (id, newContent) => {
    return Task.findById(id).then(result=> {
        result.name = newContent.name
        return result.save().then((updatedTask, saveErr)=>{
            if(saveErr){
                console.log(saveErr)
                return false
            }else{
                return updatedTask
            }
        })
    }).catch(err => console.log(err))
}

module.exports.getSpecificTask = (id)=>{
    return Task.findById(id).then((task, err)=>{
        if(err){
            console.log(err)
            return false
        }else{
            return task
        }
    })
}

module.exports.changeStatus = (id) => {
    return Task.findById(id).then(result=> {
        result.status = "complete"
        return result.save().then((updatedTask, saveErr)=>{
            if(saveErr){
                console.log(saveErr)
                return false
            }else{
                return updatedTask
            }
        })
    }).catch(err => console.log(err))
}