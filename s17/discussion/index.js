// funcs
    // Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

    //(function statement) defines a function with the specified parameters.

    // function keyword - used to defined a javascript functions
	// functionName - the function name. Functions are named to be able to use later in the code.
	// function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.

    function printName(){
        console.log("my name is slim shady")
    }

    //The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
	//It is common to use the term "call a function" instead of "invoke a function".
    printName()

    //A function can be created through function declaration by using the function keyword and adding a function name.
    //Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).

    declaredFunction() //works cuz this is a function
    //Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.
    function declaredFunction(){
        console.log('hewwo')
    }
    declaredFunction()

// fucntion expression
    // save func to variable
    //A function can also be stored in a variable. This is called a function expression.

	//A function expression is an anonymous function assigned to the variableFunction
    let varFunc = function(){
        console.log('i must have called 1000x')
    }
    varFunc()

    //You can reassign declared functions and function expressions to new anonymous functions.
    let funcExp = function funcName(){
        console.log('sorry daw')
    }
    funcExp()

    declaredFunction = function(){
        console.log('updated declaredFunction')
    }
    declaredFunction()

    funcExp= function(){
        console.log('updared funcExp')
    }
    funcExp()

    const constFunc = function(){
        console.log("consted")
    }
    constFunc()

//  function scoping
    /*	
        Scope is the accessibility (visibility) of variables.
        
        Javascript Variables has 3 types of scope:
            1. local/block scope
            2. global scope
            3. function scope
                JavaScript has function scope: Each function creates a new scope.
                Variables defined inside a function are not accessible (visible) from outside the function.
                Variables declared with var, let and const are quite similar when declared inside a function
    */
    {
        let localVar = "Armando Perez"
    }
    let globalVar = "Mr worldwide"
    console.log(globalVar)
    // console.log(localVar)
    function showNames(){
        var arth="Arthas"
        let por = "Porthas"
        const ara="Aramis"
        console.log(arth)
        console.log(por)
        console.log(ara)
    }

    showNames()

    function newFunc(){
        let name="jane"
        function nestedF(){
            let nest="john"
            console.log(name)
        }
        // console.log(nest)
        nestedF()
    }
    newFunc()
    // nestedF()

    let glName="Gill Bates"
    function newF2(){
        let nameIn="Steve jobs"
        console.log(glName)
        
        function newf3(){
            let nameIn="Elon Musk"
            console.log(glName)
        }
        newf3()
    }
    newF2()
    // console.log(nameIn)

// using alert()
    //alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.

    // alert('hello')

    function showSampleAlert(){
        alert('hello user')
    }
    // showSampleAlert()

    console.log('logs only after alrts is clsoed')
    //Notes on the use of alert():
    //Show only an alert() for short dialogs/messages to the user. 
    //Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

// prompt
    //prompt() allows us to show a small window at the of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.
    //  let samplePrompt = prompt('enter name:')
    //  console.log(typeof samplePrompt)
    //  console.log(samplePrompt)

    //prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().
    // let sampleNullPrompt = prompt("dont enter anythong")
    // console.log(sampleNullPrompt)
    // console.log(typeof sampleNullPrompt)
    //prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function.

    function printWelcome(){
        let fName=prompt("enter fname:")
        let lName=prompt("enter lname:")
        console.log("hello " + fName +" "+lName)
        console.log("wilkam")
    }
    // printWelcome()

// function naming conventions
    //Function names should be definitive of the task it will perform. It usually contains a verb.

    function getCourses(){
        let courses = ["sci", "math", "eng"]
        console.log(courses)
    }
    getCourses()

    //Avoid generic names to avoid confusion within your code.
		function get(){
			let name = "Jamie";
			console.log(name);
		};
		get();

	//Avoid pointless and inappropriate function names.
		function foo(){
			console.log(25%5);
		};
		foo();

	//Name your functions in small caps. Follow camelCase when naming variables and functions.
		function displayCarInfo(){
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");
		}
		displayCarInfo();