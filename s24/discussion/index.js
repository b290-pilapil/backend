// exponent op
    const firstNum= 8**2 //ES6
    const secondNum = Math.pow(8,2) //old way

// template literals
    // Allows to write strings without using the concatenation operator (+)
    // Greatly helps with code readability
    let name = "John"

    // dont forget ${}
    let message = "Hello "+name+"! Welcome to programming" //old way
    let newMessage = `Hello ${name}! Welcome to programming` //ES6

    // also allows:
        // multiline
        // calculations

// array destructuring
    // Allows to unpack elements in arrays into distinct variables
    // Allows us to name array elements with variables instead of using index numbers
    // Helps with code readability
    // Syntax
    //     let/const [variableName, variableName, variableName] = array;

    const fullName = ["Juan", "Dela", "Cruz"]
    const [firstName, middleName, lastName] = fullName

// object destructuring
    // Allows to unpack properties of objects into distinct variables
    // Shortens the syntax for accessing properties from objects
    // Syntax
    //     let/const {propertyName, propertyName, propertyName} = object;

    const person = {
        givenName : "Jane",
        maidenName: "Dela",
        familyName: "Cruz"
    }

    const {givenName,maidenName,familyName} = person
    // givenName: firstName
    // rename functionality

    function getFullName({givenName, maidenName, familyName}){
        console.log(`${givenName} ${maidenName} ${familyName}`)
    }
    getFullName(person)

// arrow functions
    // Compact alternative syntax to traditional functions
    // Useful for code snippets where creating functions will not be reused in any other portion of the code
    // Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
    // Syntax
    //     const variableName = () => {
    //         console.log()
    //     }
    //     let/const variableName = (parameterA, parameterB, parameterC) => {
    //         console.log();
    //     }
    const hello = ()=>{
        console.log('Hello World')
    }

    // old style
    const a = function(){
        console.log('Hello World')
    }

    const printFullName=(firstName, middleName, lastName)=>{
        console.log(`${firstName} ${middleName} ${lastName}`)
    }

    // arrow with loops

        // old style
        let students
        students.forEach(function(student){
            console.log(`${student} is a student`)
        });

        // better
        // The function is only used in the "forEach" method to print out a text with the student's names
        students.forEach(student => {
            console.log(`${student} is a student`)
        });
    
// implicit return statement
    // There are instances when you can omit the "return" statement
    // This works because even without the "return" statement JavaScript implicitly adds it for the result of the function
    const add = (x,y) =>{
        return x+y
    }
    let total = add(1,2)
    
    // one liner
    const add2 = (x,y) => x+y

// default function argument value
    // Provides a default argument value if none is provided when the function is invoked
    const greet = (name='User') => {
        return `Good morning ${name}`
    }

// class based object blueprints
    // Allows creation/instantiation of objects using classes as blueprints
    // The constructor is a special method of a class for creating/initializing an object for that class.
    // The "this" keyword refers to the properties of an object created/initialized from the class
    // By using the "this" keyword and accessing an object's property, this allows us to reassign it's values
    // Syntax
    //     class className {
    //         constructor(objectPropertyA, objectPropertyB) {
    //             this.objectPropertyA = objectPropertyA;
    //             this.objectPropertyB = objectPropertyB;
    //         }
    //     }

    // creating a class
    class Car {
        constructor(brand, name, year){
            this.brand=brand
            this.name=name
            this.year=year
        }
    }
    // The "new" operator creates/instantiates a new object with the given arguments as the values of it's properties
    // No arguments provided will create an object without any values assigned to it's properties
    // let/const variableName = new ClassName();
    let myCar = new Car()
    myCar.brand="Ford"
    myCar.name="Ranger Raptor"
    myCar.year=2021

    // Creating/instantiating a new object from the car class with initialized values
    const myNewCar = new Car("Toyota", "Vios", 2021)

    // comparison with obj instantiation instead of class
    function Pet(name, breed){
        this.breed=breed
        this.name=name
    }