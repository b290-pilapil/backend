// array methods
    // Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items.
    // Arrays can either be mutated or iterated.
    // Array mutations seek to modify the contents of an array while array iterations aim to evaluate and loop over each element in an array.

// mutator methods
    // Mutator methods are functions that "mutate" or change an array after they're created
    // These methods manipulate the original array performing various tasks such as adding and removing elements

    // push()
    // array.push(newItem, ...)
    // returns the new array length

    // pop()
    // array.pop()
    // returns the last (removed) element

    // unshift() - insert first
    // array.unshift(newItem, ...)

    // shift() - delete first
    // array.shift()
    // returns the deleted element

    // splice() - remove X elems starting at index Y
    // optional: add elems at index Y
    // array.splice(startIndex, numElementsToDelete, newElem, ...)
    // returns array of removed items

    // sort() - aplhanumeric, asc
    // array.sort()
        // mixed data types sorts by ASCII
        // NaN, empty obj before lowercase
        // (lower prio) false, null, true, undefined

    // reverse()
    // array.reverse()

// non-mutator methods
    // Non-Mutator methods are functions that do not modify or change an array after they're created
    // These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output

    // indexOf() - left to right search
    // optional startingIndex, default is 0
    // array.indexOf(item, startingIndex)
    // returns 1st match of elem, if not exist -1

    // lastIndexOf() - right to left search
    // optional startingIndex, default is 0
    // array.lastIndexOf(item, startingIndex)
    // returns 1st match of elem, if not exist -1

    // slice()
    // optional: start, default 0
    // optinal: end, default lastIndex
    // array.slice(start,end)
    // return sliced array

    // toString()
    // array.toString()
    // returns stringified array

    // concat()
    // array.concat(newArray, ...)
    // returns concatenated array 

    // join()
    // optional: separator, default comma
    // array.join(separator)
    // returns array values as string separated by the separator

// iteration methods   
    // Iteration methods are loops designed to perform repetitive tasks on arrays
    // Iteration methods loops over all items in an array.
    // Useful for manipulating array data resulting in complex tasks
    // Array iteration methods normally work with a function supplied as an argument
    // How these function works is by performing tasks that are pre-defined within an array's method

    // forEach()
    // array.forEach(element => {}) or array.forEach(function(element){})

    // map
        // Iterates on each element AND returns new array with different values depending on the result of the function's operation
        // This is useful for performing tasks where mutating/changing the elements are required
        // Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
    // array.map(element){ return })
        // return has built in push in the catcher variable

    // every()
        // isAll(condition) basically
    // array.every(function(element){ return (condition) })
    // returns boolean to the catcher variable

    // some()
        // isAny(condition) basically
    // array.some(function(element){ return (condition) })
    // returns boolean to the catcher variable

    // filter
    // array.filter(function(element){ return (condition) })
    // returns a new filtered array to the catcher variable

    // includes()
        // python: if(item in list)
    // returns a boolean to the catcher variable

    // reduce
        // cummulative of the operation in the function (ex. sum)
        // dont use = in the return
    // array.reduce(function(total, currentValue){ return (operation) })
    // returns the value of the last return inside the finction