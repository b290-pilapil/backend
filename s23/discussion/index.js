// objects
    // An object is a data type that is used to represent real world objects
    // It is a collection of related data and/or functionalities
    // In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
    // Information stored in objects are represented in a "key:value" pair
    // A "key" is also mostly referred to as a "property" of an object
    // Different data types may be stored in an object's property creating complex data structures

    // Creating objects using object initializers/literal notation
    // This creates/declares an object and also initializes/assigns it's properties upon creation
    // A cellphone is an example of a real world object
    // It has it's own properties such as name, color, weight, unit model and a lot of other things
    // Syntax
    //     let objectName = {
    //         keyA: valueA,
    //         keyB: valueB
    //     }

    // creating obects using initializer/literal notation
    let cellphone = {
        name: "Nokia 3210",
        manufactureDate: 1999
    }
    // console.log(cellphone)
    // console.log(typeof cellphone)

    // creating obects using a constructor
        // Creates a reusable function to create several objects that have the same data structure
        // This is useful for creating multiple instances/copies of an object
        // An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
        // Syntax
        //     function ObjectName(keyA, keyB) {
        //         this.keyA = keyA
        //         this.keyB = keyB
        //     
    function Laptop(name, manufactureDate){
        // This is an object
		// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters
        this.name = name,
        this.manufactureDate = manufactureDate
    }

    let laptop = new Laptop("Lenovo",2008)
    // This is a unique instance of the Laptop object
        // The "new" operator creates an instance of an object
        // Objects and instances are often interchanged because object literals (let object = {}) and instances (let object = new object) are distinct/unique objects
    // console.log(laptop)

    let myLaptop = new Laptop("MacBook Air", 2020)
        // It invokes/calls the "Laptop" function instead of creating a new object instance
        // Returns "undefined" without the "new" operator because the "Laptop" function does not have a return statement
    // console.log(myLaptop)

    // creating empty objects
        let computer = {}
        let myComputer = new Object()
        // console.log(computer)
        // console.log(myComputer)

// accessing object properties
    // dot notation
        // console.log(laptop.name)

    // sq bracket notation
        // console.log(myLaptop["name"])
    // Accessing array elements can be also be done using square brackets
    // Accessing object properties using the square bracket notation and array indexes can cause confusion
    // By using the dot notation, this easily helps us differentiate accessing elements from arrays and properties from objects
    // Object properties have names that makes it easier to associate pieces of information
        let array = [laptop, myLaptop]
            // May cause confusion for accessing array indexes
        // console.log(array[0]['name'])
            // Differentiation between accessing arrays and object properties
            // This tells us that array[0] is an object by using the dot notation
        // console.log(array[0].name)

// init/add/delete/reassign -ing object properties 
    // Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared
    // This is useful for times when an object's properties are undetermined at the time of creating them
    let car = {}
    car.name = "Honda Civic"
    // console.log(car)

    // Initializing/adding object properties using bracket notation
		// While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation
		// This also makes names of object properties to not follow commonly used naming conventions for them
    car["manufactured date"]=2019 //string
    // console.log(car)
    // console.log(car["manufactured date"])

    // deleting
    delete(car['manufactured date'])
    // console.log(car)

    // reassigning
    car.name="Volkwagen Beetle"
    // console.log(car)

    // mini activity
    function Person(name, age, personality,device){
        this.name = name,
        this.age =age,
        this.personality = personality,
        this.device=device
    }
    let me = new Person("Clyde Pilapil", 24,"IRDK","VM")
    let not_me = new Person("Alpha Bett", 24, "ABCD", "iPhone")
    // console.log(me)
    // console.log(not_me)

// object methods
    // A method is a function which is a property of an object
    // They are also functions and one of the key differences they have is that methods are functions related to a specific object
    // Methods are useful for creating object specific functions which are used to perform tasks on them
    // Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
    let person = {
        name: "Juan",
        talk: function(){
            console.log(`akos ${this.name}, kaw?`)
        }
    }
    // console.log(person)
    // person.talk()

    person.walk = function(){
        console.log(`${this.name} walked 25 steps forward`)
    }
    // console.log(person)
    // person.walk()

    let friend = {
        firstName:"Pedro",
        lastName: "Penduko",
        address: {
            city: "Manila",
            country: "Philippines"
        },
        email: ['pedro@mail.com', 'penduko101@mail.com'],
        introduce: function(){
            console.log(`hello! my name is ${this.firstName} ${this.lastName}.`)
        }
    }
    // friend.introduce()

// real world application of objects
    // Scenario
    // 1. We would like to create a game that would have several pokemon interact with each other
    // 2. Every pokemon would have the same set of stats, properties and functions

    let myPokemon = {
        name: "Pikachu",
        level: 3,
        health: 100,
        attack: 50,
        tackle: function(){
            console.log("This pokemon tackled targetPokemon")
            console.log("targetPokemon's health is now reduced to _targetPokemonhealth_")
        },
        faint: function(){
            console.log("Pokemon fainted")
        }
    }
    // console.log(myPokemon)

// Creating an object constructor instead will help with this process
function Pokemon(name, level){
    // properties
    this.name=name
    this.level=level
    this.health=2*level
    this.attack= level

    // methods
    this.tackle = function(target){
        console.log(`${this.name} tackled ${target.name}`)
        target.health=-this.attack
        if(target.health>0)
            console.log(`${target.name}'s health is now reduced to ${target.health}`)
        else{
            target.faint()
        }
    }
    this.faint = function(){
        console.log(`${this.name} fainted`)
    }
}

let pikachu = new Pokemon("Pikachu", 16)
let rattata = new Pokemon("Rattata", 8)
// console.log(typeof pikachu)
// console.log(pikachu.name)
pikachu.tackle(rattata)
