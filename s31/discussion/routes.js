const http=require('http')

const port = 4001

// app is synonymous to server
const app = http.createServer((req, res) =>{
    if(req.url=='/greeting'){
        res.writeHead(200, {"Content-Type":"text/plain"})
        res.end("Hello again")
    }else if(req.url=='/homepage'){
        res.writeHead(200, {"Content-Type":"text/plain"})
        res.end("This is the homepage")
    }else{
        res.writeHead(404, {"Content-Type":"text/plain"})
        res.end("404: Page not found")
    }
})

app.listen(port)

console.log(`Server now running at localhost:${port}.`)
