// use 'require' directive to load NodeJS
// a 'package'/'module' is a software component or part of a program that contains one/more routines/methods/functions

// 'http' module lets nodejs transfer data using HTTP
// clients (browsers and servers (node/express) communicate by exchanging individual messages
let http=require('http')

// createServer() creates an HTTP server that listens on a specified port
// a port is a virtual point where network connections start & end
// each port is associated with specific processes or serves
http.createServer(function(request, response){
    response.writeHead(200, {"Content-Typr":"text/plain"})
    // writeHead() sets the status code for the response
        //  200 -> ok/success

    response.end("Hello World")

}).listen(4000)

// prints whenever the server starts
console.log("Server running at localhost:4000")